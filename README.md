# README #

### What is this repository for? ###

* This is a Motorola 68k based single board computer, created for a major class
project. 
Planned features include:
*12Mhz clock 
*256k EEPROM and RAM
*UART to 9-pin serial and serial USB.
*CPLD based address decoding and SPI implementation
*Expansion port 'system'
*Self contained power system.
*Buttons and BlinkenLights!
* Version 0.1

### How do I get set up? ###

* This Repository contains Kicad project files and libraries. Routing was completed with freerouter. Completed Gerber files are also included.
* VHDL code for CPLD was made using Xilinx ISE and loaded with iMPACT
* BOM and project planning spreadsheet will be added soon!

### Usage guidelines ###

You can use these files however you like with these exceptions.

-You may not sell my work.
-If you are doing a similar project for school, don't plagiarize! Besides, its much more fun to do it yourself.   

### Who do I talk to? ###

Contact me if you have questions, comments, or suggestions regarding this project.