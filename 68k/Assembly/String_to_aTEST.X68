*-----------------------------------------------------------
* Title      :
* Written by :
* Date       :
* Description:
*-----------------------------------------------------------
    ORG    $1000
START:                  ; first instruction of program

* Put program code here
STRING_TO_MEM
    MOVEM.L  A2-A3/D4-D6,-(SP)
    CLR.W    D6
    LEA      regbuf,A2
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D4
        ROL.W       #8,D4
        ROL.W       #4,D4
        ROL.L       #4,D4
        ADD.B       #$01,D6
    ENDW
    CLR     D6
    WHILE.W D6 <NE> #$02 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D5
        ROL.W       #8,D5
        ROL.W       #4,D5
        ROL.L       #4,D5
        ADD.B       #$01,D6
    ENDW
    SWAP    D5
    MOVE.W  D5,D4
    ROL.W   #8,D4
    ROR.L   #8,D4
    MOVE.L  D4,D3
    MOVEM.L  (SP)+,A2-A3/D4-D6    
    MOVE.L  D3,(A3) *move the converted contents of D3 to the location addressed by a3
    RTS    
    *converts byte contents of D3 from ascii (1 byte) to hex (1 digit)
ASCII_TO_HEX
    CMP.B   #$30,D3
    BLT     FORM_FAIL
    CMP.B   #$39,D3
    BGT     ASCII_TO_HEX_ALPHA
    SUB.B   #$30,D3   
    RTS
ASCII_TO_HEX_ALPHA
    CMP.B   #$41,D3
    BLT     FORM_FAIL
    CMP.B   #$46,D3
    BGT     FORM_FAIL
    SUB.B   #$37,D3
    RTS

FORM_FAIL
    NOP
    JMP FORM_FAIL

    SIMHALT             ; halt simulator

* Put variables and constants here
regbuf dc.b '15FFFF', $0d
    END    START        ; last line of source

*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~4~
