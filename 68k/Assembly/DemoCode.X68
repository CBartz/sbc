
START       ORG		$000000 
            DC.L    SUPER_STACK
            DC.L    MAIN   

MAIN		ORG     $000100
*            MOVE.L  #$00000000,D4 *Initialize data and address registers
*            MOVE.L  #$00000000,D5
*            MOVE.L  #$00000000,D6
*            MOVE.L  #$00000000,D7
*            MOVE.L  #$00000000,A4
*            MOVE.L  #$00000000,A5
*            MOVE.L  #$00000000,A6
            JSR		INIT_DUART	;initialize DUART
LOOPER     
            LEA     menu1,A2 *Load string to be output
            JSR     PUT_LINE 
            
            LEA     linebuf,A2 *Load location for stringbuffer
            JSR     GET_LINE
            
            JSR     COMM_TABLE
            JMP     LOOPER


INIT_DUART  LEA   	DUART,A0 *A0 points to base DUART address
; Software reset:
    		MOVE.B 	#$30,CRA(A0) 	*Reset TxA 
    		MOVE.B 	#$20,CRA(A0) 	*Reset RxA 
			MOVE.B 	#$10,CRA(A0) 	*Reset MRA pointer
			MOVE.B 	#$80,ACR(A0) 	*selects baud rate set 2
			MOVE.B 	#BAUD,CSRA(A0)  *set baudrate of Rx/Tx
; Initialization:
    		MOVE.B  #$13,MR1A(A0)   *8-bits, no parity, 1 stop bit
; This is the most important register to set in the 68681 DUART.
; 07 sets: Normal mode, CTS and RTS disabled, stop bit length = 1
; For testing load $#47 to enable auto-echo
    		MOVE.B #$07,MR2A(A0) 
    		MOVE.B #$05,CRA(A0) *enable Tx and Rx
    		RTS

*gets char from DUART and places on D2
GET_CHAR
            MOVEM.L     D0-D1/A0,-(SP)
            LEA         DUART,A0
INPUT_POLL  MOVE.B      SRA(A0),D1
            BTST        #RxRDY,D1
            BEQ         INPUT_POLL
            MOVE.B      RBA(A0),D2
            JSR         PUT_CHAR *Echo back the char that was input
            MOVEM.L     (SP)+,D0-D1/A0
            RTS
*gets char from D2 and places on DUART
PUT_CHAR
            MOVEM.L     D0-D1/A0,-(SP)
            LEA         DUART,A0
OUTPUT_POLL MOVE.B      SRA(A0),D1
            BTST        #TxRDY,D1
            BEQ         OUTPUT_POLL
            MOVE.B      D2,TBA(A0)
            MOVEM.L     (SP)+,D0-D1/A0
            RTS
*Load first address location of string into A2 and call put_line
PUT_LINE
        MOVEM.L D2,-(SP)
        MOVE.B  #$00,D2 *Clear D2
        WHILE.B D2 <NE> CR DO
            MOVE.B      (A2)+,D2 *Load contents of memory indicated by the contents of A2 into D2  
            JSR         PUT_CHAR    
        ENDW
        MOVE.B      LF,D2 *Output a line-feed
        JSR         PUT_CHAR
        MOVEM.L (SP)+,D2 
        RTS
        
*Load A2 to the location you want the string to be put, then call Get_line
GET_LINE
        MOVEM.L D2,-(SP)
        MOVE.B  #$00,D2 *Clear D2
        WHILE.B D2 <NE> CR DO
            JSR     GET_CHAR
            MOVE.B  D2,(A2)+
        ENDW
        MOVE.B  CR,(A2)+ *Write final CR to A2
        MOVE.B      LF,D2 *Output a line-feed
        JSR         PUT_CHAR
        MOVE.B      CR,D2 *Output a carrage return
        JSR         PUT_CHAR
        MOVEM.L (SP)+,D2 *Restore D2
        RTS
        
*After a getline that returns a command in the buffer, call COMM_TABLE to find appropriate subroutine for excecution (getline must modify linebuf)
COMM_TABLE
    LEA     linebuf,A2
*Memory modify subroutine    
    LEA     mmod,A3
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        *Call appropriate subroutine
        JSR MEMORY_MODIFY
        RTS
    ENDI
*Address Register modify subroutine
    LEA     amod,A3
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        *Call appropriate subroutine
        JSR ADDRESS_REG_MODIFY
        RTS
    ENDI
*S-Record loading routine
    LEA     loads,A3
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        LEA     loads,A2
        JSR     PUT_LINE
        *Call appropriate subroutine
        RTS
    ENDI
*Data Register modify subroutine
    LEA     dmod,A3
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        JSR WOOP
        *Call appropriate subroutine
        RTS
    ENDI
*Failure to find command
    LEA     commandError,A2 *Load string to be output
    JSR     PUT_LINE     
    RTS

*Load the address of a two strings into A2(from buffer) and A3(from table), if the two match buffer(A2) is filled with ACK ($06).
COMP_STRING
    CMPM.B    (A2)+,(A3)+  
    BNE       COMP_STRING_FAIL
    CMPI.B    #$0d,(A2) *Check for carrage return in buffer
    BNE       COMP_STRING
    CMPI.B    #$0d,(A3) *Check for carrage return in table, if the CR is not in the same position as both, then fail
    BNE       COMP_STRING_FAIL
    *If it gets past here, your strings are complete matches
    LEA       linebuf,A2 *load the linebuffer to A2
    MOVE.B    #$06,(A2) *write an acknowlege to the linebuffer 
    RTS
COMP_STRING_FAIL
    LEA       linebuf,A2 *load the linebuffer to A2
    RTS   

*converts byte contents of D3 from ascii (1 byte) to hex (1 digit)
ASCII_TO_HEX
    CMP.B   #$30,D3
    BLT     FORM_FAIL
    CMP.B   #$39,D3
    BGT     ASCII_TO_HEX_ALPHA
    SUB.B   #$30,D3   
    RTS
ASCII_TO_HEX_ALPHA
    CMP.B   #$41,D3
    BLT     FORM_FAIL
    CMP.B   #$46,D3
    BGT     FORM_FAIL
    SUB.B   #$37,D3
    RTS

*converts contents of D3 from hex (1 digit) to ascii (1 byte)     
HEX_TO_ASCII
    CMP.B   #$00,D3
    BLT     FORM_FAIL
    CMP.B   #$09,D3
    BGT     HEX_TO_ASCII_ALPHA
    ADD.B   #$30,D3   
    RTS
HEX_TO_ASCII_ALPHA
    CMP.B   #$0A,D3
    BLT     FORM_FAIL
    CMP.B   #$0F,D3
    BGT     FORM_FAIL
    ADD.B   #$37,D3
    RTS
    
FORM_FAIL
    MOVEM.L A2,-(SP)
    LEA     ffail,A2
    JMP     PUT_LINE
    MOVEM.L (SP)+,A2
    RTS
    
*Converts the contents of D3 to a string regbuf
D_TO_STRING_DONT_USE
    MOVEM.L  A3/D4-D6,-(SP)      *Store registers to stack
    MOVE.W   #$0000,D6
    MOVE.W   #$0000,D5              *Set counters
    LEA      regbuf,A3
    MOVE.L   D3,D4                  *Move input register into working register
    CLR.L      D3
    WHILE.W D6 <NE> #$20 DO
        SWAP     D4
        MOVE.W   D4,D3              *move the word to d3
        SWAP     D3                 *swap so we can do single nybble operations
        WHILE.W D5 <NE> #$04 DO
            ROL.L    #$04,D3        *rotate
            JSR      HEX_TO_ASCII
            MOVE.B   D3,(A3)+
            CLR.B    D3  
            ADD.W    #$01,D5
        ENDW
        MOVE.W  #$0000,D5 *reset counter
        ADD.W   #$10,D6
    ENDW
    MOVEM.L  (SP)+,A3/D4-D6
    RTS

*Gets contents of regbuff and converts it to register contents D3
STRING_TO_DONT_USE
    MOVEM.L  A3/D4-D6,-(SP)
    CLR      D6
    LEA      regbuf,A3
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A3)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D4
        ROL.W       #8,D4
        ROL.W       #4,D4
        ROL.L       #4,D4
        ADD.B       #$01,D6
    ENDW
    CLR     D6
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A3)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D5
        ROL.W       #8,D5
        ROL.W       #4,D5
        ROL.L       #4,D5
        ADD.B       #$01,D6
    ENDW
    SWAP    D5
    MOVE.W  D5,D4
    MOVE.L  D4,D3
    MOVEM.L  (SP)+,A3/D4-D6
    RTS
    
*Reverses the contents of register D3

REVERSE_REG
    MOVEM.L D4,-(SP)
    ROL.W   #8,D3
    SWAP    D3
    ROL.W   #8,D3
    MOVE.L  D3,D4
    AND.L   #$f0f0f0f0,d3
    EOR.L   D3,D4
    LSR.L   #4,D3
    LSL.L   #4,D4
    OR.L    D4,D3
    MOVEM.L (SP)+,D4
    RTS


ADDRESS_REG_MODIFY
    LEA     aregmenu1,A2
    JSR     PUT_LINE
    LEA     linebuf,A2
    JSR     GET_LINE
    *begin break table
    LEA     address4,A3 *check A4
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        MOVE.L  A4,A3 *if user input d0, then load d0 into register for conversion to string
        JSR     OUT_CURRENT_A
        JSR     IN_CURRENT_A
        MOVE.L  A3,A4
        JSR     OUT_CURRENT_A
        RTS
    ENDI
    LEA     address5,A3 *check A5
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        MOVE.L  A5,A3 *if user input d0, then load d0 into register for conversion to string
        JSR     OUT_CURRENT_A
        JSR     IN_CURRENT_A
        MOVE.L  A3,A5
        JSR     OUT_CURRENT_A
        RTS
    ENDI
    LEA     address6,A3 *check A6
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        MOVE.L  A6,A3 *if user input d0, then load d0 into register for conversion to string
        JSR     OUT_CURRENT_A
        JSR     IN_CURRENT_A
        MOVE.L  A3,A6
        JSR     OUT_CURRENT_A
        RTS
    ENDI
    LEA     address7,A3 *check A7
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        MOVE.L  A7,A3 *if user input d0, then load d0 into register for conversion to string
        JSR     OUT_CURRENT_A
        JSR     IN_CURRENT_A
        MOVE.L  A3,A7
        JSR     OUT_CURRENT_A
        RTS
    ENDI

 
    RTS

OUT_CURRENT_A *Outputs the contents of D3 to string and transmits
    JSR     A_TO_STRING
    LEA     regbuf,A2
    JSR     PUT_LINE
    RTS

IN_CURRENT_A *gets user input and places it on A3
    LEA     regbuf,A2
    JSR     GET_LINE
    JSR     STRING_TO_A_REG
    RTS
*takes input from regbuf and converts it to 2 byte A3
STRING_TO_A_REG
    MOVEM.L  A2-A3/D4-D6,-(SP)
    CLR.W    D6
    LEA      regbuf,A2
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D4
        ROL.W       #8,D4
        ROL.W       #4,D4
        ROL.L       #4,D4
        ADD.B       #$01,D6
    ENDW
    CLR     D6
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D5
        ROL.W       #8,D5
        ROL.W       #4,D5
        ROL.L       #4,D5
        ADD.B       #$01,D6
    ENDW
    SWAP    D5
    MOVE.W  D5,D4
    MOVE.L  D4,D3
    MOVEM.L  (SP)+,A2-A3/D4-D6    
    MOVE.L  D3,A3 *move the converted contents of D3 to the location addressed by a3
    RTS

*Converts the contents of A3 to a sring regbuff    
A_TO_STRING
    MOVEM.L  A3/D4-D6,-(SP)      *Store registers to stack
    MOVE.W   #$0000,D6
    MOVE.W   #$0000,D5              *Set counters
    MOVE.L   A3,D4
    LEA      regbuf,A3              *Move input register into working register
    CLR.L      D3
    WHILE.W D6 <NE> #$20 DO
        SWAP     D4
        MOVE.W   D4,D3              *move the word to d3
        SWAP     D3                 *swap so we can do single nybble operations
        WHILE.W D5 <NE> #$04 DO
            ROL.L    #$04,D3        *rotate
            JSR      HEX_TO_ASCII
            MOVE.B   D3,(A3)+
            CLR.B    D3  
            ADD.W    #$01,D5
        ENDW
        MOVE.W  #$0000,D5 *reset counter
        ADD.W   #$10,D6
    ENDW
    MOVE.B    $0d,(A3) *put a carrage return on the end?
    MOVEM.L  (SP)+,A3/D4-D6
    RTS
    
*****************************************

MEMORY_MODIFY
    MOVEM.L  A2-A3/D4-D6,-(SP)
    LEA     mmodmenu1,A2 *get user to input address of the memory that they want to modify
    JSR     PUT_LINE
    LEA     regbuf,A2
    JSR     GET_LINE
    JSR     STRING_TO_A *Change user input to usable address in A3
    MOVE.L  (A3),D3 *move the contents of the memory at the user specified address to d3 for conversion to string
    JSR     D_TO_STRING *convert to string in regbuf
    LEA     regbuf,A2
    JSR     PUT_LINE *output regbuf.
**************************At this point we have output current contents of memory locaton
    *Now to write to memory
    LEA     regbuf,A2
    JSR     GET_LINE *user specifies new contents
    JSR     STRING_TO_MEM
    MOVEM.L  (SP)+,A2-A3/D4-D6
    RTS
    
*Gets contents of regbuff and converts it to register contents A3 (A memory address, 6 bytes)
STRING_TO_A    
    MOVEM.L  D4-D6,-(SP)
    CLR.W    D6
    LEA      regbuf,A3
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A3)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D4
        ROL.W       #8,D4
        ROL.W       #4,D4
        ROL.L       #4,D4
        ADD.B       #$01,D6
    ENDW
    CLR     D6
    WHILE.W D6 <NE> #$02 DO
        MOVE.B      (A3)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D5
        ROL.W       #8,D5
        ROL.W       #4,D5
        ROL.L       #4,D5
        ADD.B       #$01,D6
    ENDW
    SWAP    D5
    MOVE.W  D5,D4
    ROL.W   #8,D4
    CLR.B   D4
    ROR.L   #8,D4     
    MOVE.L  D4,A3
    MOVEM.L  (SP)+,D4-D6
    RTS

*Gets contents of regbuff and converts to register contents D3, then writes it to the location addressed in A3
STRING_TO_MEM
    MOVEM.L  A2-A3/D4-D6,-(SP)
    CLR.W    D6
    LEA      regbuf,A2
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D4
        ROL.W       #8,D4
        ROL.W       #4,D4
        ROL.L       #4,D4
        ADD.B       #$01,D6
    ENDW
    CLR     D6
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D5
        ROL.W       #8,D5
        ROL.W       #4,D5
        ROL.L       #4,D5
        ADD.B       #$01,D6
    ENDW
    SWAP    D5
    MOVE.W  D5,D4
    MOVE.L  D4,D3
    MOVEM.L  (SP)+,A2-A3/D4-D6    
    MOVE.L  D3,(A3) *move the converted contents of D3 to the location addressed by a3
    RTS
    
    *****************************************
WOOP
    LEA     dregmenu1,A2
    JSR     PUT_LINE
    LEA     linebuf,A2
    JSR     GET_LINE
    *begin break table
    LEA     data4,A3 *check D4
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        MOVE.L  D4,D3 *if user input d0, then load d0 into register for conversion to string
        JSR     OUT_CURRENT_D
        JSR     IN_CURRENT_D
        MOVE.L  D3,D4
        RTS
    ENDI
    *this is one module... just so you know. copy paste for more options
    LEA     data5,A3 *check D5
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        MOVE.L  D5,D3 
        JSR     OUT_CURRENT_D
        JSR     IN_CURRENT_D
        MOVE.L  D3,D5
        RTS
    ENDI
    LEA     data6,A3 *check D6
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        MOVE.L  D6,D3 
        JSR     OUT_CURRENT_D
        JSR     IN_CURRENT_D
        MOVE.L  D3,D6
        RTS
    ENDI
    LEA     data7,A3 *check D7
    JSR     COMP_STRING
    IF.B    (A2) <EQ> #$06 THEN
        MOVE.L  D7,D3 
        JSR     OUT_CURRENT_D
        JSR     IN_CURRENT_D
        MOVE.L  D3,D7
        RTS
    ENDI
*Failure to find command
    LEA     commandError,A2 *Load string to be output
    JSR     PUT_LINE     
    RTS
    
;*************************************************

OUT_CURRENT_D *Outputs the contents of D3 to string and transmits
    JSR     D_TO_STRING
    LEA     regbuf,A2
    JSR     PUT_LINE
    RTS

IN_CURRENT_D *gets user input and places it on D3
    LEA     regbuf,A2
    JSR     GET_LINE
    JSR     STRING_TO_D
    RTS
    
*Converts the contents of D3 to a sring regbuff    
D_TO_STRING
    MOVEM.L  A3/D4-D6,-(SP)      *Store registers to stack
    MOVE.W   #$0000,D6
    MOVE.W   #$0000,D5              *Set counters
    MOVE.L   D3,D4
    LEA      regbuf,A3              *Move input register into working register
    CLR.L      D3
    WHILE.W D6 <NE> #$20 DO
        SWAP     D4
        MOVE.W   D4,D3              *move the word to d3
        SWAP     D3                 *swap so we can do single nybble operations
        WHILE.W D5 <NE> #$04 DO
            ROL.L    #$04,D3        *rotate
            JSR      HEX_TO_ASCII
            MOVE.B   D3,(A3)+
            CLR.B    D3  
            ADD.W    #$01,D5
        ENDW
        MOVE.W  #$0000,D5 *reset counter
        ADD.W   #$10,D6
    ENDW
    MOVE.B    $0d,(A3) *put a carrage return on the end?
    MOVEM.L  (SP)+,A3/D4-D6
    RTS
    
*Gets contents of regbuff and converts it to register contents D3 (A memory address, 6 bytes)
STRING_TO_D
    MOVEM.L  A2-A3/D4-D6,-(SP)
    CLR.W    D6
    LEA      regbuf,A2
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D4
        ROL.W       #8,D4
        ROL.W       #4,D4
        ROL.L       #4,D4
        ADD.B       #$01,D6
    ENDW
    CLR     D6
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D5
        ROL.W       #8,D5
        ROL.W       #4,D5
        ROL.L       #4,D5
        ADD.B       #$01,D6
    ENDW
    SWAP    D5
    MOVE.W  D5,D4
    MOVE.L  D4,D3
    MOVEM.L  (SP)+,A2-A3/D4-D6
    RTS
************************************************************
S_RECORD
    LEA     srec,A2
    JSR     PUT_LINE
    WHILE <T> DO *infinite loop
SLOOP   LEA     linebuf,A2 
        JSR     GET_LINE_SREC *get a single line of srecord           
        *check S0
        IF.W  (A2) <EQ> #$5330 THEN
            *IF THE LINE BEGINS IN S0
            *Do nothing, we dont care about S0!
            JMP SLOOP
        ENDI
        LEA     linebuf,A2 
        LEA     srec_S1,A3 *check S1
        IF.W  (A2) <EQ> #$5331 THEN
            *IF THE LINE BEGINS IN S1
            ADDA.L  #$2,A2 *get past the 'S1'
            JSR     TWO_ASCII_TO_HEX_COUNTER *puts length to end in D3
            MOVE.L  D3,D4 *D4 will be used for a counter
            JSR     FOUR_ASCII_TO_HEX_ADDRESS *puts address to write to in A3
            SUB.L   #$02,D4 *Subtract number of bytes in address from counter
            WHILE   D4 <NE> #$00 DO
                *read data into (A3) and count down D3 until D3=0
                MOVE.B  (A2)+,(A3)+
                MOVE.B  (A2)+,(A3)+
                SUB.L   #$01,D3
            ENDW
            *Ignore checksum
        JMP SLOOP
        ENDI
 ************************************************************       
        LEA     linebuf,A2 
        LEA     srec_S2,A3 *check S2
        IF.W  (A2) <EQ> #$5332 THEN
            *IF THE LINE BEGINS IN S2
            ADDA.L  #$2,A2 *get past the 'S2'
            JSR     TWO_ASCII_TO_HEX_COUNTER *puts length to end in D3
            MOVE.L  D3,D4 *D4 will be used for a counter
            JSR     SIX_ASCII_TO_HEX_ADDRESS *puts address to write to in A3
            SUB.L   #$03,D4 *Subtract number of bytes in address from counter
            WHILE   D4 <NE> #$00 DO
                *read data into (A3) and count down D3 until D3=0
                MOVE.B  (A2)+,(A3)+
                MOVE.B  (A2)+,(A3)+
                SUB.L   #$01,D3
            ENDW
            *Ignore checksum
        JMP SLOOP
        ENDI
**************************************************************
        LEA     linebuf,A2 
        LEA     srec_S3,A3 *check S3
        IF.W  (A2) <EQ> #$5333 THEN
        *IF THE LINE BEGINS IN S3
            ADDA.L  #$2,A2 *get past the 'S3'
            JSR     TWO_ASCII_TO_HEX_COUNTER *puts length to end in D3
            MOVE.L  D3,D4 *D4 will be used for a counter
            JSR     EIGHT_ASCII_TO_HEX_ADDRESS *puts address to write to in A3
            SUB.L   #$04,D4 *Subtract number of bytes in address from counter
            WHILE   D4 <NE> #$00 DO
                *read data into (A3) and count down D3 until D3=0
                MOVE.B  (A2)+,(A3)+
                MOVE.B  (A2)+,(A3)+
                SUB.L   #$01,D3
            ENDW
            *Ignore checksum
        
        JMP SLOOP
        ENDI
        LEA     linebuf,A2 
        LEA     srec_S8,A3 *check S8
        IF.W  (A2) <EQ> #$5338 THEN
        *IF THE LINE BEGINS IN S8
            ADDA.L  #$2,A2 *get past the 'S8'
            JSR     TWO_ASCII_TO_HEX_COUNTER *puts length to end in D3
            MOVE.L  D3,D4 *D4 will be used for a counter
            JSR     EIGHT_ASCII_TO_HEX_ADDRESS *puts address to write to in A3
            *The start location for the SRecord is now in A3
            LEA     srecwin,A2
            JSR     PUT_LINE
            JSR     (A3)
            RTS
        JMP SLOOP
        ENDI
        LEA     ffail,A2
        JSR     PUT_LINE



    ENDW    
    
    
    
*Load A2 to the location you want the string to be put, then call Get_line
GET_LINE_SREC
        MOVEM.L D2,-(SP)
        MOVE.B  #$00,D2 *Clear D2
        WHILE.W D2 <NE> LF DO
            JSR     GET_CHAR
            MOVE.B  D2,(A2)+
        ENDW
        MOVE.B  #$0a,(A2)+ *put a line feed on the end
        JSR     GET_CHAR *This is to clear out the carrage return at the end of the line
        MOVEM.L (SP)+,D2 *Restore D2
        RTS
*Converts next two bytes from string pointed to by A2 from ascii to hex and places on D3        
TWO_ASCII_TO_HEX_COUNTER
        CLR.L   D3
        MOVE.B  (A2)+,D3 *will contain upper byte
        JSR     ASCII_TO_HEX
        MOVE.B  D3,D4
        ROL.W   #$08,D4 *shift into uper byte position
        MOVE.B  (A2)+,D3 *will contain lower byte
        JSR     ASCII_TO_HEX
        ROL.B   #$04,D4
        ROR.L   #$04,D4
        MOVE.B  D4,D3
        RTS
        
*Converts next four bytes from string pointed to by A2 from ascii to hex and places on A3        
FOUR_ASCII_TO_HEX_ADDRESS
    CLR.L   D3  
    MOVEM.L  D4-D6,-(SP)
    CLR.W    D6
    CLR.L    D4
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D4
        ROL.W       #8,D4
        ROL.W       #4,D4
        ROL.L       #4,D4
        ADD.B       #$01,D6
    ENDW
    CLR.W   D6
    SWAP    D4
    MOVE.L  D4,A3
    MOVEM.L  (SP)+,D4-D6
    RTS

*Converts next six bytes from string pointed to by A2 from ascii to hex and places on A3   
SIX_ASCII_TO_HEX_ADDRESS    
    MOVEM.L  D4-D6,-(SP)
    CLR.W    D6
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D4
        ROL.W       #8,D4
        ROL.W       #4,D4
        ROL.L       #4,D4
        ADD.B       #$01,D6
    ENDW
    CLR     D6
    WHILE.W D6 <NE> #$02 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D5
        ROL.W       #8,D5
        ROL.W       #4,D5
        ROL.L       #4,D5
        ADD.B       #$01,D6
    ENDW
    SWAP    D5
    MOVE.W  D5,D4
    ROL.W   #8,D4
    CLR.B   D4
    ROR.L   #8,D4     
    MOVE.L  D4,A3
    MOVEM.L  (SP)+,D4-D6
    RTS
    
*Converts next 8 bytes from string pointed to by A2 from ascii to hex and places on A3   
EIGHT_ASCII_TO_HEX_ADDRESS
    MOVEM.L  A2-A3/D4-D6,-(SP)
    CLR.W    D6
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D4
        ROL.W       #8,D4
        ROL.W       #4,D4
        ROL.L       #4,D4
        ADD.B       #$01,D6
    ENDW
    CLR     D6
    WHILE.W D6 <NE> #$04 DO
        MOVE.B      (A2)+,D3
        JSR         ASCII_TO_HEX
        MOVE.B      D3,D5
        ROL.W       #8,D5
        ROL.W       #4,D5
        ROL.L       #4,D5
        ADD.B       #$01,D6
    ENDW
    SWAP    D5
    MOVE.W  D5,D4
    MOVE.L  D4,D3
    MOVEM.L  (SP)+,A2-A3/D4-D6    
    MOVE.L  D3,A3 *move the converted contents of D3 to a3
    RTS

    SIMHALT
    
*-----------------------------------------------------------
SUPER_STACK     EQU     $10FFFE
*SP is synenonymus A7
*-----------------------------------------------------------
* Note, most DUART configurations are ignorant of UDS/LDS 
* and thus are only active on odd addresses (since the lower 
* data lines are usually run to the DUART data bus)
; *******************************************************************
; Constants: (Note the offsets to account for no A0)
DUART   EQU $200000     loaded in A0 when needed, regs are offsets
MR1A    EQU     1   *Mode Register1
MR2A    EQU     1   *points here after MR1A is set
SRA     EQU     3   *Status Register (read)
CSRA    EQU     3   *Clock Select Register
CRA     EQU     5   *Command Register
TBA     EQU     7   *Transfer Holding Register
RBA     EQU     7   *Receive Holding Register
ACR     EQU     9   *Auxiliary control register
RxRDY   EQU     0   *Recieve ready bit position
TxRDY   EQU     2   *Transmit ready bit position
BAUD    EQU     $BB *baud rate value = 9600 baud
; *******************************************************************
*linebuf is a 64 byte long section of memory for storing input char strings. The 65th spot is occupied with an end of text char.
linebuf EQU    $110000
regbuf  EQU    $120000 *Used for storing user entered register or data longword entries 
; *********************************************************************
CR      dc.b    $0d *Carrage Return
LF      dc.b    $0a *Line Feed
ESC     dc.b    $1b *Escape Char
ETX     dc.b    $03 *End of text
ACK     dc.b    $06 *Acknowlege
; *********************************************************************
; Commands:
mmod   dc.b    'mmod', $0d
amod     dc.b    'amod', $0d
dmod     dc.b    'drmod', $0d
loads       dc.b    'loads', $0d
*********************************************************************
menu1   dc.b    'BucketOS', $0d
commandError    dc.b    'Invalid Command', $0d
ffail   dc.b    'Invalid input form', $0d
srec    dc.b    'Ready for S-Record', $0d
srecwin    dc.b    'Executing S-Record', $0d
; *********************************************************************
dregmenu1   dc.b    'Enter the data register you want to modify', $0d 
aregmenu1   dc.b    'Enter the address register you want to modify', $0d
mmodmenu1   dc.b    'Enter the address in memory to modify', $0d 
;*************************************************
*REGISTER STRING TABLE
address4    dc.b    'A4', $0d
address5    dc.b    'A5', $0d
address6    dc.b    'A6', $0d
address7    dc.b    'A7', $0d

data3       dc.b    'D3', $0d
data4       dc.b    'd4', $0d
data5       dc.b    'D5', $0d
data6       dc.b    'D6', $0d
data7       dc.b    'D7', $0d

srec_S0     dc.b    'S0'
srec_S1     dc.b    'S1'
srec_S2     dc.b    'S2'
srec_S3     dc.b    'S3'
srec_S8     dc.b    'S8'
**************************************************************************

    END START







*~Font name~Courier New~
*~Font size~10~
*~Tab type~1~
*~Tab size~4~
