----------------------------------------------------------------------------------
-- Engineer: Caleb Bartz
-- 
-- Create Date:    19:37:54 03/10/2015  
-- Project Name: 68k Glue Logic and SPI implementation
-- Description: 
--
-- Revision: 
-- Revision 0.01 - File Created
----------------------------------------------------------------------------------
library IEEE;
use IEEE.STD_LOGIC_1164.ALL;

-- Uncomment the following library declaration if using
-- arithmetic functions with Signed or Unsigned values
--use IEEE.NUMERIC_STD.ALL;

-- Uncomment the following library declaration if instantiating
-- any Xilinx primitives in this code.
--library UNISIM;
--use UNISIM.VComponents.all;

entity main is
Port(
	IPL	: out STD_LOGIC_VECTOR (2 downto 0);
	FC		: in	STD_LOGIC_VECTOR (2 downto 0);
	VMA	: in	STD_LOGIC;
	VPA	: out STD_LOGIC;
	E		: in	STD_LOGIC;
	CLK_IN: in	STD_LOGIC;
	BERR	: out	STD_LOGIC;
	DTACK	: out	STD_LOGIC;
	HALT  : inout STD_LOGIC;
	UDS	: in	STD_LOGIC;
	LDS	: in	STD_LOGIC;
	IND	: out STD_LOGIC_VECTOR (3 downto 1) := "111";
	A		: in	STD_LOGIC_VECTOR (18 downto 1);
	AD		: in	STD_LOGIC_VECTOR (5 downto 1);
	D		: inout STD_LOGIC_VECTOR (15 downto 0);
	RW		: in	STD_LOGIC;
	SW1	: in	STD_LOGIC;
	SW2	: in	STD_LOGIC;
	AS		: in	STD_LOGIC;
	CS_UART	: out STD_LOGIC;
	IAK_UART	: out STD_LOGIC;
	RAM_HB_CS: out STD_LOGIC;
	RAM_LB_CS: out STD_LOGIC;
	ROM_HB_CS: out STD_LOGIC;
	SS		: out STD_LOGIC_VECTOR (4 downto 1);
	IRQ_UART	: out STD_LOGIC;
	ROM_LB_CS: out STD_LOGIC;
	MISO	: out STD_LOGIC;
	MOSI	: out STD_LOGIC);
end main;

architecture Behavioral of main is


begin
--Indicator Lights
blinken: process(CLK_IN)begin
	IND <= "111";
end process;


--Addresss decoding process
decode: process(AD, RW, UDS, LDS)begin
	ROM_LB_CS	<= (NOT LDS AND NOT AD(1) AND NOT AD(2) AND NOT AD(3) AND NOT AD(4) AND AD(5)); --00001 LB
	ROM_HB_CS	<= (NOT UDS AND NOT AD(1) AND NOT AD(2) AND NOT AD(3) AND NOT AD(4) AND AD(5)); --00001 UB
	RAM_LB_CS	<= (NOT LDS AND NOT AD(1) AND NOT AD(2) AND NOT AD(3) AND AD(4) AND NOT AD(5)); --00010 LB
	RAM_HB_CS	<= (NOT UDS AND NOT AD(1) AND NOT AD(2) AND NOT AD(3) AND AD(4) AND NOT AD(5)); --00010 UB
	CS_UART		<= (NOT AD(1) AND NOT AD(2) AND NOT AD(3) AND AD(4) AND AD(5)); ------------------00011
	SS(1)			<= (NOT AD(1) AND NOT AD(2) AND AD(3) AND NOT AD(4) AND NOT AD(5)); --------------00100
	SS(2)			<= (NOT AD(1) AND NOT AD(2) AND AD(3) AND NOT AD(4) AND AD(5)); ------------------00101
	SS(3)			<= (NOT AD(1) AND NOT AD(2) AND AD(3) AND AD(4) AND NOT AD(5)); ------------------00110
	SS(4)			<= (NOT AD(1) AND NOT AD(2) AND AD(3) AND AD(4) AND AD(5)); ----------------------00111
end process;

--DTACK Generator

--Reset sequence

--Memory Verification
end Behavioral;

