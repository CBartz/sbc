PCBNEW-LibModule-V1  2/7/2015 3:13:48 PM
# encoding utf-8
Units mm
$INDEX
SBC_Button
$EndINDEX
$MODULE SBC_Button
Po 0 0 0 15 54D65E53 00000000 ~~
Li SBC_Button
Sc 0
AR 
Op 0 0 0
T0 0 -5 1 1 0 0.15 N V 21 N "SBC_Button"
T1 0 5 1 1 0 0.15 N V 21 N "VAL**"
DS -4 -4 4 -4 0.15 21
DS 4 -4 4 4 0.15 21
DS 4 4 -4 4 0.15 21
DS -4 4 -4 -4 0.15 21
$PAD
Sh "1" C 1.5 1.5 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po -2.5 0
$EndPAD
$PAD
Sh "2" C 1.5 1.5 0 0 0
Dr 1 0 0
At STD N 00E0FFFF
Ne 0 ""
Po 2.5 0
$EndPAD
$EndMODULE SBC_Button
$EndLIBRARY
